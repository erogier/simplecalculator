//
//  ViewController.swift
//  SimpleCalculator
//
//  Created by Eddy Rogier on 07/11/2017.
//  Copyright © 2017 Eddy R. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var nb1:String = "" // number 01
    var nb2:String = "" // number 02
    var som:Double?     // result
    var ope:String = "" // + / * - containt operator arithmetic
    //Flag
    var nb1IsEmpty:Bool = true
    var nb2IsEmpty:Bool = true
    var opeIsEmpty:Bool = true
    var equalIsPressed:Bool = false
    var dotIswritted:Bool = false
    var calcIsDone:Bool = false
    var negIsWritted1:Bool = false
    var negIsWritted2:Bool = false
    
    @IBOutlet weak var aff: UILabel!
    @IBOutlet weak var btnAC: UIButton!
    @IBOutlet weak var nbtOpeDiv: UIButton!
    @IBOutlet weak var nbtOpeMul: UIButton!
    @IBOutlet weak var nbtOpeSou: UIButton!
    @IBOutlet weak var nbtOpeAdd: UIButton!
    //\/ -------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        som = 0
        aff.text = formatResult(som: som!)
        debug()
    }
    
    // MARK: - NUMBER BUTTON 0 1 2 3 4 5 6 7 8 9
    @IBAction func actionNb(_ sender: UIButton) {
        // ʕ•ᴥ•ʔ verification if calcule is finish and if the button equal is pressed
        if calcIsDone && equalIsPressed {
            print("      ʕ•ᴥ•ʔ calcIsDone", calcIsDone)
            for i in 0...9 {
                print(i)
                if sender.titleLabel?.text == String(i) {
                    print("Find !", i)
                    nb1IsEmpty = true
                    nb2IsEmpty = true
                    nb1 = ""
                    nb2 = ""
                    ope = ""
                    calcIsDone = false
                    equalIsPressed = false
                    break
                }
            }
        }
        
        // test if nb1 and nb2 is fill in order for the futur operation
        if nb1IsEmpty {
            formatLeadingMultiZero(number: &nb1, sender: sender.titleLabel!.text!)
            debug()
        } else if !nb1IsEmpty && !opeIsEmpty{
            formatLeadingMultiZero(number: &nb2, sender: sender.titleLabel!.text!)
            highlightingButtonOff()
            debug()
        }
        
        // Change text button AC by C if nb1 is fill
        if nb1 != "" {
            btnAC.setTitle("C", for: UIControlState.normal)
        }
    }
    
    // MARK: - OPERATEUR  "+"  "-"  "*"  "/"
    @IBAction func actionOpe(_ sender: UIButton) {
        testEmptyActionOp()
        // if first number is empty so i don't fill the variable that containt the operator
        if nb1IsEmpty {
            ope = ""
        } else {
            nb1IsEmpty = false
            ope = (sender.titleLabel?.text!)! // brut data without format
            controlAndFormatOperatorArithmetique(sender)
            
            // ʕ•ᴥ•ʔ calculate with only operator.
            if !nb1IsEmpty && !opeIsEmpty && !nb2IsEmpty {
                // define the calcul
                switch ope {
                case "-":
                    print("      ʕ•ᴥ•ʔ subtraction")
                    calculInOpe(ope: subtraction)
                    break
                case "+":
                    print("      ʕ•ᴥ•ʔ addition")
                    calculInOpe(ope: additionnal)
                    break
                case "/":
                    print("      ʕ•ᴥ•ʔ division")
                    calculInOpe(ope: division)
                    break
                case "*":
                    print("      ʕ•ᴥ•ʔ mutiplication")
                    calculInOpe(ope: multiplication)
                    break
                default:
                    break
                }
            }//ENDIF
        }
    }
    
    // MARK: - OPERATEUR  "="
    @IBAction func actionEqual(_ sender: UIButton) {
        if nb2 != "" && nb2 != "-"{
            nb2IsEmpty = false
        } else {
            nb2IsEmpty = true
        }
        
        // ʕ•ᴥ•ʔ if all variable are full then we process calcule
        if !nb1IsEmpty && !nb2IsEmpty && !opeIsEmpty {
            // defines the operation
            switch ope {
            case "-":
                print("      ʕ•ᴥ•ʔ soustraction")
                calculInActionEqual(ope:subtraction)
                break
            case "+":
                print("      ʕ•ᴥ•ʔ addition")
                calculInActionEqual(ope:additionnal)
                break
            case "/":
                print("      ʕ•ᴥ•ʔ division")
                calculInActionEqual(ope:division)
                break
            case "*":
                print("      ʕ•ᴥ•ʔ mutiplication")
                calculInActionEqual(ope:multiplication)
                break
            default:
                break
            }
        } else {
            print("some variable are not filled")
        }
    }
    
    
    // MARK: - DOT BTN FOR THE DECIMAL "."
    @IBAction func dotbutton(_ sender: UIButton) {
        if calcIsDone && equalIsPressed {
            print("      ʕ•ᴥ•ʔ calcIsDone", calcIsDone)
            nb1IsEmpty = true
            nb2IsEmpty = true
            nb1 = ""
            nb2 = ""
            ope = ""
            
            calcIsDone = false
            equalIsPressed = false
            dotIswritted = false // pas sur
        }
        
        if nb1IsEmpty {
            if !dotIswritted && nb1 != ""{
                nb1.append( (sender.titleLabel?.text)! )
                aff.text = nb1
                dotIswritted = true
                debug()
            } else if sender.titleLabel?.text == "." && dotIswritted == false {
                nb1.append( "0." )
                aff.text = nb1
                dotIswritted = true
                debug()
            } else {
                debug()
            }
        } else if !nb1IsEmpty && !opeIsEmpty{
            
            if !dotIswritted && nb2 != "" {
                nb2.append( (sender.titleLabel?.text)! )
                aff.text = nb2
                dotIswritted = true
                debug()
            } else if sender.titleLabel?.text == "." && dotIswritted == false {
                nb2.append( "0." )
                aff.text = nb2
                dotIswritted = true
                debug()
            } else {
                debug()
            }
        }// ENDIF
    }
    
    // MARK: - CORRECTION BTN "AC"
    @IBAction func actionAC(_ sender: UIButton) {
        if nb1 != "" || nb2 != ""{
            nb1 = ""
            nb2 = ""
            ope = ""
            print("**************** nb1 is already emptied : \(nb1), ope is already emptied \(ope) , nb2 is already emptied : \(nb2) *******************")
            sender.setTitle("AC", for: UIControlState.normal)
            // ʕ•ᴥ•ʔ emptying view
            aff.text = String(describing: 0)
            nb1IsEmpty = true
            dotIswritted = false
            // ʕ•ᴥ•ʔ reset flag
            nb1IsEmpty = true
            nb2IsEmpty = true
            opeIsEmpty = true
            equalIsPressed = false
            dotIswritted = false
            calcIsDone = false
            negIsWritted1 = false
            negIsWritted2 = false
            highlightingButtonOff()
            debug()
        } else {
            print("      ʕ•ᴥ•ʔ nb1 or nb2 already emptied", nb1, nb2)
        }
    }
    
    // MARK: - add positive or negative sign in front of the number "+/-"
    @IBAction func actionNegativePositive(_ sender: UIButton) {
        if nb1IsEmpty {
            if !negIsWritted1{
                // when u clik the dot button this one that adds a zero in front of the dot
                if nb1 == "" {
                    nb1.append("-0")
                } else if nb1 == "0" {
                    nb1.insert("-", at: nb1.startIndex)
                } else {
                    nb1.insert("-", at: nb1.startIndex)
                } //end
                aff.text = nb1
                negIsWritted1 = true
                debug()
            } else {
                nb1.remove(at: nb1.startIndex)
                aff.text = nb1
                negIsWritted1 = false
                debug()
            }
        } else {
            if !negIsWritted2{
                // when u clik the dot button this one that adds a zero in front of the dot
                if nb2 == "" {
                    nb2.append("-0")
                } else if nb2 == "0" {
                    nb2.insert("-", at: nb2.startIndex)
                } else {
                    nb2.insert("-", at: nb2.startIndex)
                } //end
                aff.text = nb2
                negIsWritted2 = true
                debug()
            } else {
                nb2.remove(at: nb2.startIndex)
                aff.text = nb2
                negIsWritted2 = false
                debug()
            }
        }
    }
    
    // MARK: - PerCent "%"
    @IBAction func actionPerCent(_ sender: UIButton) {
        debug()
        if nb1IsEmpty {
            if nb1 != "" && nb1 != "-" {
                let sum:Double = Double(nb1)! / 100
                nb1 = String(sum)
                print(sum)
                print(nb1)
                aff.text = nb1
            } else {
                print("      ʕ•ᴥ•ʔ nothing nb1 %")
            }
            
        } else if !nb1IsEmpty && !opeIsEmpty{
            if nb2 != "" && nb2 != "-" {
                let sum:Double = Double(nb2)! / 100
                nb2 = String(sum)
                print(sum)
                print(nb2)
                aff.text = nb2
            } else {
                print("      ʕ•ᴥ•ʔ nothing nb2 %")
            }
        } //ENDIF
    }

}

extension ViewController {
    
    /** Remove the last zero decimal on the sum of type Double */
    func formatResult(som:Double) -> String {
        let tempVar = String(format: "%g", som)
        return tempVar
    }
    
    /** show if the variables are empty or not */
    func debug(){
        print("\n")
        print("|")
        print("'------------>")
        print("              START -----------------------------")
        print("              ʕ•ᴥ•ʔ nb1 => \(nb1)")
        print("              ʕ•ᴥ•ʔ ope => \(String(describing: ope))")
        print("              ʕ•ᴥ•ʔ nb2 => \(nb2)")
        print("              END -------------------------------")
        print("\n")
        print( """
            var nb1IsEmpty:Bool       = \(nb1IsEmpty)
            var nb2IsEmpty:Bool       = \(nb2IsEmpty)
            var opeIsEmpty:Bool       = \(opeIsEmpty)
            var equalIsPressed:Bool = \(equalIsPressed)
            var dotIswritted:Bool   = \(dotIswritted)
            var calcIsDone:Bool     = \(calcIsDone)
            var negIsWritted1:Bool  = \(negIsWritted1)
            var negIsWritted2:Bool  = \(negIsWritted2)
            """)
    }
    
    /** let 1 zero if several clicks on the zero button */
    func formatLeadingMultiZero(number: inout String, sender:String) {
        if number == "0"  {
            if sender != "0" {
                number = ""
                number.append(sender)
                aff.text = number
            }
        } else if number == "-0" {
            if sender != "0" {
                number = "-"
                number.append(sender)
                aff.text = number
            }
        } else {
            number.append(sender)
            aff.text = number
        }
    }
    
    func initalisationNegativeSign(result:Double){
        if som! < 0.0 {
            negIsWritted1 = true
        } else {
            negIsWritted1 = false
        }
    }
    
    // -------------------------MARK: - **** SECTION method OPERATOR *** ---------------------------------------------------------
    /** do not uses the operation if the inputs are empty */
    fileprivate func testEmptyActionOp(){
        if nb1 != "" && nb1 != "-" && nb1 != "-."{
            nb1IsEmpty = false
        } else {
            nb1IsEmpty = true
        }
        if nb2 != "" && nb2 != "-" {
            nb2IsEmpty = false
        } else {
            nb2IsEmpty = true
        }
    }
    
    fileprivate func resetFlagInActionOp() {
        opeIsEmpty = false
        dotIswritted = false
    }
    /** 01 Controle and format sign arithmetic */
    fileprivate func controlAndFormatOperatorArithmetique(_ sender: UIButton) {
        switch (sender.titleLabel?.text!)! {
        case "÷":
            ope = "/"
            resetFlagInActionOp()
            highlightingButton(sender: sender, withMachtOperator: "÷")
            debug()
            break
        case "-":
            ope = "-"
            resetFlagInActionOp()
            highlightingButton(sender: sender, withMachtOperator: "-")
            debug()
            break
        case "+":
            ope = "+"
            resetFlagInActionOp()
            highlightingButton(sender: sender, withMachtOperator: "+")
            
            debug()
            break
        case "×":
            ope = "*"
            resetFlagInActionOp()
            highlightingButton(sender: sender, withMachtOperator: "×")
            debug()
            break
        default:
            break
        }
    }
    //\/ -------------------------
    typealias OperationFunction = (Double,Double)-> Double
    
    fileprivate func additionnal(numberA:Double, numberB:Double) -> Double{
        return numberA + numberB
    }
    fileprivate func multiplication(numberA:Double, numberB:Double) -> Double{
        return numberA * numberB
    }
    fileprivate func division(numberA:Double, numberB:Double) -> Double{
        return numberA / numberB
    }
    fileprivate func subtraction(numberA:Double, numberB:Double) -> Double{
        return numberA - numberB
    }
    
    fileprivate func calculate(a:Double, b:Double,operation : OperationFunction) -> Double{
        return operation(a, b)
    }
    
    /** method that performs the normal calculation of an operation */
    fileprivate func calculInOpe(ope:OperationFunction){
        if equalIsPressed {
            nb2 = ""
            equalIsPressed = false
        } else {
            som = calculate(a: Double(nb1)!, b: Double(nb2)!, operation: ope)
        }
        // update the view
        aff.text = formatResult(som: som!)
        print("result = ",som!)
        // Check the flags because we put the content of the sum for example (nb1 + nb2) in nb1
        // so if the value is positive or negative, then we change the flag depending on the result to avoid the crash.
        initalisationNegativeSign(result: som!)
        // fills nb1 for continue the calculus or not
        nb1 = String(describing: som!)
        nb2 = ""
        negIsWritted2 = false
    }
    
    /** Verification calcul and security if calculs with buttuon equal */
    fileprivate func calculInActionEqual(ope:OperationFunction){
        //som =  Double(nb1)! - Double(nb2)!
        som = calculate(a: Double(nb1)!, b: Double(nb2)!, operation: ope)
        aff.text = formatResult(som: som!)
        debug()
        print("Result is =>",som!)
        nb1 = String(describing: som!)
        print("... nb1 : \(nb1) | nb2 : \(nb2) | ope : \(self.ope)")
        //nb2 = ""
        equalIsPressed = true
        calcIsDone     = true
        
        negIsWritted1 = false
        negIsWritted2 = false
    }
 
    fileprivate func highlightingButton(sender:UIButton,withMachtOperator  match:String){
        print("high light")
        if sender.titleLabel?.text! == match {
            highlightingButtonOff()
            UIView.animate(withDuration: 0.3, animations: {
                sender.backgroundColor = UIColor.white
                sender.setTitleColor(UIColor(red: 255.0/255.0, green: 153.0/255.0, blue: 69.0/255.0, alpha: 1.0), for: UIControlState.normal)
            })
        } else {
            highlightingButtonOff()
        }
    }
    
    func settingTurnOffHighLigh (btn:UIButton){
        btn.backgroundColor = UIColor(red: 255.0/255.0, green: 153.0/255.0, blue: 69.0/255.0, alpha: 1.0)
        btn.setTitleColor(UIColor.white, for: UIControlState.normal)
    }
    
    func highlightingButtonOff(){
        print("Off light")
        settingTurnOffHighLigh(btn: nbtOpeSou)
        settingTurnOffHighLigh(btn: nbtOpeDiv)
        settingTurnOffHighLigh(btn: nbtOpeAdd)
        settingTurnOffHighLigh(btn: nbtOpeMul)
    }

}


